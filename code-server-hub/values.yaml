## Global Docker image parameters
## Please, note that this will override the image parameters, including dependencies, configured to use the global value
## Current available global Docker image parameters: imageRegistry and imagePullSecrets
##
global:
# imagePullSecrets:
#   - myRegistryKeySecretName
  jupyterhub: {}
#   imageRegistry: myRegistryName
#   imagePullSecrets:
#     - myRegistryKeySecretName
#   storageClass: myStorageClass

## String to partially override postgresql.fullname template (will maintain the release name)
##
# nameOverride:

## String to fully override postgresql.fullname template
##
# fullnameOverride:

custom: {}

hub:
  service:
    type: ClusterIP
    annotations: {}
    ports:
      nodePort:
    loadBalancerIP:
  baseUrl: /
  cookieSecret:
  publicURL:
  initContainers: []
  uid: 1000
  fsGid: 1000
  nodeSelector: {}
  concurrentSpawnLimit: 64
  consecutiveFailureLimit: 5
  activeServerLimit:
  deploymentStrategy:
    ## type: Recreate
    ## - sqlite-pvc backed hubs require the Recreate deployment strategy as a
    ##   typical PVC storage can only be bound to one pod at the time.
    ## - JupyterHub isn't designed to support being run in parallell. More work
    ##   needs to be done in JupyterHub itself for a fully highly available (HA)
    ##   deployment of JupyterHub on k8s is to be possible.
    type: Recreate
  db:
    type: sqlite-pvc
    upgrade:
    pvc:
      annotations: {}
      selector: {}
      accessModes:
        - ReadWriteOnce
      storage: 1Gi
      subPath:
      storageClassName:
    url:
    password:
  labels: {}
  annotations: {}
  extraConfig:
    codeserverhub: |
      from kubespawner.spawner import KubeSpawner as KSO
      from tornado import gen
      class KubeSpawner(KSO):
          def get_pod_manifest(self):
              if self.extra_containers:
                  for container in self.extra_containers:
                      if "name" in container and container["name"] == "oauth":
                          container["env"] = [ {'name': k, 'value': v} for k, v in (self.get_env() or {}).items()]
                      if "name" in container and container["name"] == "nginx":
                          container["env"] = [ {'name': k, 'value': v} for k, v in (self.get_env() or {}).items()]
              return super(KubeSpawner, self).get_pod_manifest()
      c.JupyterHub.spawner_class = KubeSpawner
  extraEnv: {}
  extraContainers: []
  extraVolumes: []
  extraVolumeMounts: []
  image:
    name: jupyterhub/k8s-hub
    tag: '0.9.1'
  resources:
    requests:
      cpu: 200m
      memory: 512Mi
    limits:
      cpu: 200m
      memory: 512Mi
  services: {}
  pdb:
    enabled: true
    minAvailable: 1
  networkPolicy:
    enabled: false
    ingress: []
    egress:
      - to:
          - ipBlock:
              cidr: 0.0.0.0/0
  allowNamedServers: false
  namedServerLimitPerUser:
  authenticatePrometheus:
  redirectToServer:
  shutdownOnLogout:
  templatePaths: []
  templateVars: {}
  livenessProbe:
    enabled: false
    initialDelaySeconds: 30
    periodSeconds: 10
  readinessProbe:
    enabled: true
    initialDelaySeconds: 0
    periodSeconds: 10
  # existingSecret: existing-secret

proxy:
  secretToken: ''
  deploymentStrategy:
    ## type: Recreate
    ## - JupyterHub's interaction with the CHP proxy becomes a lot more robust
    ##   with this configuration. To understand this, consider that JupyterHub
    ##   during startup will interact a lot with the k8s service to reach a
    ##   ready proxy pod. If the hub pod during a helm upgrade is restarting
    ##   directly while the proxy pod is making a rolling upgrade, the hub pod
    ##   could end up running a sequence of interactions with the old proxy pod
    ##   and finishing up the sequence of interactions with the new proxy pod.
    ##   As CHP proxy pods carry individual state this is very error prone. One
    ##   outcome when not using Recreate as a strategy has been that user pods
    ##   have been deleted by the hub pod because it considered them unreachable
    ##   as it only configured the old proxy pod but not the new before trying
    ##   to reach them.
    type: Recreate
    ## rollingUpdate:
    ## - WARNING:
    ##   This is required to be set explicitly blank! Without it being
    ##   explicitly blank, k8s will let eventual old values under rollingUpdate
    ##   remain and then the Deployment becomes invalid and a helm upgrade would
    ##   fail with an error like this:
    ##
    ##     UPGRADE FAILED
    ##     Error: Deployment.apps "proxy" is invalid: spec.strategy.rollingUpdate: Forbidden: may not be specified when strategy `type` is 'Recreate'
    ##     Error: UPGRADE FAILED: Deployment.apps "proxy" is invalid: spec.strategy.rollingUpdate: Forbidden: may not be specified when strategy `type` is 'Recreate'
    rollingUpdate:
  service:
    type: LoadBalancer
    labels: {}
    annotations: {}
    nodePorts:
      http:
      https:
    loadBalancerIP:
    loadBalancerSourceRanges: []
  chp:
    image:
      name: jupyterhub/configurable-http-proxy
      tag: 4.2.1
    livenessProbe:
      enabled: true
      initialDelaySeconds: 30
      periodSeconds: 10
    readinessProbe:
      enabled: true
      initialDelaySeconds: 0
      periodSeconds: 10
    resources:
      requests:
        cpu: 200m
        memory: 512Mi
      limits:
        cpu: 200m
        memory: 512Mi
  secretSync:
    image:
      name: jupyterhub/k8s-secret-sync
      tag: '0.9.1'
    resources: {}
  labels: {}
  nodeSelector: {}
  pdb:
    enabled: true
    minAvailable: 1
  https:
    enabled: true
    type: secret
    #type:  manual, offload, secret, cert-manager
    manual:
      key:
      cert:
    secret:
      name: ''
      key: tls.key
      crt: tls.crt
    kubernetesDNSDomain: cluster.local
    hosts: []
  networkPolicy:
    enabled: false
    ingress: []
    egress:
      - to:
          - ipBlock:
              cidr: 0.0.0.0/0

auth:
  type: dummy
  whitelist:
    users:
  admin:
    access: true
    users:
  dummy:
    password:
  ldap:
    dn:
      search: {}
      user: {}
    user: {}
  state:
    enabled: false
    cryptoKey:

singleuser:
  extraTolerations: []
  nodeSelector: {}
  extraNodeAffinity:
    required: []
    preferred: []
  extraPodAffinity:
    required: []
    preferred: []
  extraPodAntiAffinity:
    required: []
    preferred: []
  networkTools:
    image:
      name: jupyterhub/k8s-network-tools
      tag: '0.9.1'
  cloudMetadata:
    enabled: false
    ip: 169.254.169.254
  networkPolicy:
    enabled: false
    ingress: []
    egress:
    # Required egress is handled by other rules so it's safe to modify this
      - to:
          - ipBlock:
              cidr: 0.0.0.0/0
              except:
                - 169.254.169.254/32
  events: true
  extraAnnotations: {}
  extraLabels:
    hub.jupyter.org/network-access-hub: 'true'
  extraEnv: {}
  lifecycleHooks: {}
  initContainers: []
  extraContainers: []
  uid: 1000
  fsGid: 100
  serviceAccountName:
  storage:
    type: dynamic
    extraLabels: {}
    extraVolumes:
      - name: shm-volume
        emptyDir:
          medium: Memory
    extraVolumeMounts: []
    static:
      pvcName:
      subPath: '{username}'
    capacity: 20Gi
    homeMountPath: /home/coder
    dynamic:
      storageClass:
      pvcNameTemplate: claim-{username}{servername}
      volumeNameTemplate: volume-{username}{servername}
      storageAccessModes: [ReadWriteOnce]
  image:
    name: registry.gitlab.com/captnbp/code-server-hub/code-server
    tag: 1.3.5
    pullPolicy: IfNotPresent
    # imagePullSecret: secretName
  startTimeout: 300
  cpu:
    limit:
    guarantee:
  memory:
    limit: 2G
    guarantee: 1G
  extraResource:
    limits: {}
    guarantees: {}
  cmd: jupyterhub-singleuser
  defaultUrl:
  extraPodConfig: {}
  profileList:
    - description: code-server
      default: true
      display_name: VScode
      kubespawner_override:
        cmd:
          - code-server
        image: registry.gitlab.com/captnbp/code-server-hub/code-server:1.3.5
        uid: 1000
        gid: 1000
        fs_gid: 1000
        privileged: false
        extra_containers:
          - name: nginx
            image: registry.gitlab.com/captnbp/code-server-hub/nginx:1.3.5
            resources:
              requests:
                cpu: 100m
                memory: 50Mi
              limits:
                cpu: 100m
                memory: 100Mi
            securityContext:
              allowPrivilegeEscalation: false
              runAsUser: 101
              runAsGroup: 101
              privileged: false
              runAsNonRoot: true
              capabilities:
                drop:
                  - ALL
          - name: oauth
            image: registry.gitlab.com/captnbp/code-server-hub/oauth:1.3.5
            ports:
              - containerPort: 9095
                name: oauth
                protocol: TCP
            resources:
              requests:
                cpu: 100m
                memory: 50Mi
              limits:
                cpu: 100m
                memory: 100Mi
            securityContext:
              allowPrivilegeEscalation: false
              runAsUser: 33
              runAsGroup: 33
              privileged: false
              runAsNonRoot: true
              capabilities:
                drop:
                  - ALL
          - name: browser
            image: registry.gitlab.com/captnbp/code-server-hub/browser:1.3.5
            ports:
              - containerPort: 6080
                name: browser
                protocol: TCP
            resources:
              requests:
                cpu: 100m
                memory: 200Mi
              limits:
                cpu: 2
                memory: 2Gi
            securityContext:
              allowPrivilegeEscalation: false
              runAsUser: 1000
              runAsGroup: 1000
              privileged: false
              runAsNonRoot: true
              capabilities:
                drop:
                  - ALL
            volumeMounts:
              - name: volume-{username}
                mountPath: /home/coder
              - name: shm-volume
                mountPath: /dev/shm
    - description: code-server-anaconda
      default: false
      display_name: VScode + Anaconda
      kubespawner_override:
        cmd:
          - code-server
        image: registry.gitlab.com/captnbp/code-server-hub/code-server-anaconda:1.3.5
        extra_containers:
          - name: nginx
            image: registry.gitlab.com/captnbp/code-server-hub/nginx:1.3.5
            resources:
              requests:
                cpu: 100m
                memory: 50Mi
              limits:
                cpu: 100m
                memory: 100Mi
            securityContext:
              allowPrivilegeEscalation: false
              runAsUser: 101
              runAsGroup: 101
              privileged: false
              runAsNonRoot: true
              capabilities:
                drop:
                  - ALL
          - name: oauth
            image: registry.gitlab.com/captnbp/code-server-hub/oauth:1.3.5
            ports:
              - containerPort: 9095
                name: oauth
                protocol: TCP
            resources:
              requests:
                cpu: 100m
                memory: 50Mi
              limits:
                cpu: 100m
                memory: 100Mi
            securityContext:
              allowPrivilegeEscalation: false
              runAsUser: 33
              runAsGroup: 33
              privileged: false
              runAsNonRoot: true
              capabilities:
                drop:
                  - ALL

scheduling:
  userScheduler:
    enabled: false
    replicas: 2
    logLevel: 4
    ## policy:
    ## Allows you to provide custom YAML/JSON to render into a JSON policy.cfg,
    ## a configuration file for the kube-scheduler binary.
    ## NOTE: The kube-scheduler binary in the kube-scheduler image we are
    ## currently using may be version bumped. It would for example happen if we
    ## increase the lowest supported k8s version for the helm chart. At this
    ## point, the provided policy.cfg may require a change along with that due
    ## to breaking changes in the kube-scheduler binary.
    policy: {}
    image:
      name: gcr.io/google_containers/kube-scheduler-amd64
      tag: v1.13.12
    nodeSelector: {}
    pdb:
      enabled: true
      minAvailable: 1
    resources:
      requests:
        cpu: 50m
        memory: 256Mi
  podPriority:
    enabled: false
    globalDefault: false
    defaultPriority: 0
    userPlaceholderPriority: -10
  corePods:
    nodeAffinity:
      matchNodePurpose: prefer
  userPods:
    nodeAffinity:
      matchNodePurpose: prefer

ingress:
  enabled: false
  annotations:
    kubernetes.io/ingress.class: nginx
    cert-manager.io/cluster-issuer: letsencrypt-prod
  hosts:
    - host: chart-example.local
      paths: []
  tls:
    - secretName: chart-example-tls
      hosts:
        - chart-example.local

cull:
  enabled: false
  users: false
  removeNamedServers: false
  timeout: 3600
  every: 600
  concurrency: 10
  maxAge: 0


debug:
  enabled: false


## Pod Service Account
## ref: https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/
##
serviceAccount:
  enabled: true
  create: true

## Pod Security Policy
## ref: https://kubernetes.io/docs/concepts/policy/pod-security-policy/
##
psp:
  create: true

## Creates role for ServiceAccount
## Required for PSP
##
rbac:
  enabled: true
  create: true

## Configure metrics exporter
##
metrics:
  enabled: false
  service:
    annotations:
      prometheus.io/scrape: 'true'
      prometheus.io/port: '8081'
  serviceMonitor:
    enabled: false
    additionalLabels: {}
    # namespace: monitoring
    # interval: 30s
    # scrapeTimeout: 10s
  ## Custom PrometheusRule to be defined
  ## The value is evaluated as a template, so, for example, the value can depend on .Release or .Chart
  ## ref: https://github.com/coreos/prometheus-operator#customresourcedefinitions
  ##
  prometheusRule:
    enabled: false
    additionalLabels: {}
    namespace: ""
    rules: []